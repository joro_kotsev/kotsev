### Prerequisites
The following items should be installed in your system:
* Java 8 or newer.
* Chrome and Firefox in order to try both browsers
* Prefered IDE Tool - InteliJ 

###InteliJ
* Import project via File -> Import -> Maven -> Existing Maven project
* Generate report via Intelij Extract Result tool
* Run tests via right button and click the Run Button


### Tests
* Tests are in the folder - TakeAway\src\test\java\tests
* TakeAwayTestOne - this test covers Case1 from the task
* TakeAwayTestTwo - this test covers Case2 from the task
* Paralel Testing - this test is divided by 2 exactly the same
tests covering Case1. Running it will start in paralel , but one 
with browser Chrome, and the other with browser Firefox. 

### Generating Reports
* Reports can be generated automatically with the Intelij Extract Result tool - the
icon is on the Run view
* There are generated html files, which can be viewed by browser:
	- Test Results - ParalelelTesting.html
	- Test Results - TakeAwayTestOne.html
	- Test Results - TakeAwayTestTwo.html
* After each test you can overwrite the exact testing report





