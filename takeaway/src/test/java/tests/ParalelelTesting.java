package tests;

import base.BaseTest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;

@Execution(ExecutionMode.CONCURRENT)
public class ParalelelTesting extends BaseTest {

    @Test
    public void takeAwayTestChrome() throws InterruptedException{
        initBaseTestChrome();
        takeAwayWebApplication.mainPage().openMainPage();
        takeAwayWebApplication.takeAwayPage().searchTextField("8888");
        takeAwayWebApplication.takeAwayPage().clickShowButton();
        takeAwayWebApplication.takeAwayPage().selectAlphaAddress();
        takeAwayWebApplication.takeAwayPage().verifyRestaurantsList();
        takeAwayWebApplication.takeAwayPage().selectRestaurantBasBurger();
        takeAwayWebApplication.takeAwayPage().selectHamburger();
        takeAwayWebApplication.takeAwayPage().clickBurgerPrice();
        takeAwayWebApplication.takeAwayPage().sleepMiliseconds(2000);
        takeAwayWebApplication.takeAwayPage().clickOrderButton();

        takeAwayWebApplication.addressPage().enterAddress();
        takeAwayWebApplication.addressPage().enterPostcode();
        takeAwayWebApplication.addressPage().enterCity();
        takeAwayWebApplication.addressPage().enterName();
        takeAwayWebApplication.addressPage().enterEmail();
        takeAwayWebApplication.addressPage().enterPhoneNumber();
        takeAwayWebApplication.addressPage().selectDeliveryOption("As soon as possible");
        takeAwayWebApplication.addressPage().selectPay("€ 8,00");
        takeAwayWebApplication.addressPage().clickOrderAndPayButton();
        takeAwayWebApplication.addressPage().getReferenceNumber();
        tearDownBaseTest();
    }

    @Test
    public void takeAwayTestFirefox() throws InterruptedException{
        initBaseTestFirefox();

        takeAwayWebApplication.mainPage().openMainPage();
        takeAwayWebApplication.takeAwayPage().searchTextField("8888");
        takeAwayWebApplication.takeAwayPage().clickShowButton();
        takeAwayWebApplication.takeAwayPage().selectAlphaAddress();
        takeAwayWebApplication.takeAwayPage().verifyRestaurantsList();
        takeAwayWebApplication.takeAwayPage().selectRestaurantBasBurger();
        takeAwayWebApplication.takeAwayPage().sleepMiliseconds(2000);
        takeAwayWebApplication.takeAwayPage().selectHamburger();
        takeAwayWebApplication.takeAwayPage().sleepMiliseconds(2000);
        takeAwayWebApplication.takeAwayPage().clickBurgerPrice();
        takeAwayWebApplication.takeAwayPage().clickOrderButton();

        takeAwayWebApplication.addressPage().enterAddress();
        takeAwayWebApplication.addressPage().enterPostcode();
        takeAwayWebApplication.addressPage().enterCity();
        takeAwayWebApplication.addressPage().enterName();
        takeAwayWebApplication.addressPage().enterEmail();
        takeAwayWebApplication.addressPage().enterPhoneNumber();
        takeAwayWebApplication.addressPage().selectDeliveryOption("As soon as possible");
        takeAwayWebApplication.addressPage().selectPay("€ 8,00");
        takeAwayWebApplication.addressPage().clickOrderAndPayButton();
        takeAwayWebApplication.addressPage().getReferenceNumber();
        tearDownBaseTest();
    }

}
