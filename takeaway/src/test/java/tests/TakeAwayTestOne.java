package tests;

import base.BaseTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class TakeAwayTestOne extends BaseTest {

    @BeforeEach
    public void init(){
        initBaseTestChrome();
    }

    @Test
    public void takeAwayTest() throws InterruptedException{

        takeAwayWebApplication.mainPage().openMainPage();
        takeAwayWebApplication.takeAwayPage().searchTextField("8888");
        takeAwayWebApplication.takeAwayPage().clickShowButton();
        takeAwayWebApplication.takeAwayPage().selectAlphaAddress();
        takeAwayWebApplication.takeAwayPage().verifyRestaurantsList();
        takeAwayWebApplication.takeAwayPage().selectRestaurantBasBurger();
        takeAwayWebApplication.takeAwayPage().selectHamburger();
        takeAwayWebApplication.takeAwayPage().clickBurgerPrice();
        takeAwayWebApplication.takeAwayPage().clickOrderButton();

        takeAwayWebApplication.addressPage().enterAddress();
        takeAwayWebApplication.addressPage().enterPostcode();
        takeAwayWebApplication.addressPage().enterCity();
        takeAwayWebApplication.addressPage().enterName();
        takeAwayWebApplication.addressPage().enterEmail();
        takeAwayWebApplication.addressPage().enterPhoneNumber();
        takeAwayWebApplication.addressPage().selectDeliveryOption("As soon as possible");
        takeAwayWebApplication.addressPage().selectPay("€ 8,00");
        takeAwayWebApplication.addressPage().clickOrderAndPayButton();
        takeAwayWebApplication.addressPage().getReferenceNumber();



    }

    @AfterEach
    public void tearDown(){
       tearDownBaseTest();
    }

}
