package base;

import core.TakeAwayWebApplication;

public class BaseTest {

    public TakeAwayWebApplication takeAwayWebApplication = new TakeAwayWebApplication();

    public BaseTest(){
    }

    public void initBaseTestChrome() {
        takeAwayWebApplication.browser().initChrome();
    }

    public void initBaseTestFirefox() {
        takeAwayWebApplication.browser().initFirefox();
    }

    public void tearDownBaseTest(){
        takeAwayWebApplication.browser().tearDown();
    }
}





