package pages;

import base.BaseTools;
import base.GlobalProperties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AddressPage extends BaseTools {

    //public final Logger LOGGER = LoggerFactory.getLogger(AddressPage.class);

    WebDriver webDriver;

    public AddressPage(WebDriver driver){
        super(driver);
    }

    //WebElements-------------------------

    @FindBy(how = How.NAME, using = "address")
    public WebElement address;

    @FindBy(how = How.NAME, using = "postcode")
    public WebElement postcode;

    @FindBy(how = How.NAME, using = "town")
    public WebElement town;

    @FindBy(how = How.NAME, using = "surname")
    public WebElement surname;

    @FindBy(how = How.NAME, using = "email")
    public WebElement email;

    @FindBy(how = How.NAME, using = "phonenumber")
    public WebElement phonenumber;

    @FindBy(how = How.NAME, using = "deliverytime")
    public WebElement deliverytime;

    @FindBy(how = How.ID, using = "ipayswith")
    public WebElement ipayswith;

    @FindBy(how = How.XPATH, using = "/html/body/main/div[2]/div/div/div/div[2]/div[2]/input")
    public WebElement orderAndPayButton;

    @FindBy(how = How.XPATH, using = "/html/body/div[4]/div/div/div/div[1]")
    public WebElement referenceNumber;




    //Methods--------------------------------

    public  void enterAddress(){
        LOGGER.info("Entering adress " + GlobalProperties.getInstance().getAddress());
        typeText(address, GlobalProperties.getInstance().getAddress());
    }

    public void enterPostcode(){
        LOGGER.info("Entering postcode " + GlobalProperties.getInstance().getPostcode());
        typeText(postcode, GlobalProperties.getInstance().getPostcode());
    }

    public void enterCity(){
        LOGGER.info("Entering city " + GlobalProperties.getInstance().getCity());
        typeText(town, GlobalProperties.getInstance().getCity());
    }

    public void enterName(){
        LOGGER.info("Entering name " + GlobalProperties.getInstance().getName());
        typeText(surname, GlobalProperties.getInstance().getName());
    }

    public void enterEmail(){
        LOGGER.info("Entering email " + GlobalProperties.getInstance().getEmail());
        typeText(email, GlobalProperties.getInstance().getEmail());
    }

    public void enterPhoneNumber(){
        LOGGER.info("Entering phonenumber " + GlobalProperties.getInstance().getPhonenum());
        typeText(phonenumber, GlobalProperties.getInstance().getPhonenum());
    }

     public void selectDeliveryOption(String text){
        LOGGER.info("Selecting delivery option" + text);
        deliverytime.sendKeys(text);
    }

    public void selectPay(String text){
        LOGGER.info("Selecting pay amount" + text);
        ipayswith.sendKeys(text);
    }

    public void clickOrderAndPayButton(){
        LOGGER.info("Clicking the order and pay button");
        click(orderAndPayButton);
    }

    public void getReferenceNumber(){
        LOGGER.info("!!! " + referenceNumber.getText() + " !!!");

    }




}
