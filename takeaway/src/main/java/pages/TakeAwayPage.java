package pages;

import base.BaseTools;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TakeAwayPage extends BaseTools {

   // public final Logger LOGGER = LoggerFactory.getLogger(TakeAwayPage.class);

    WebDriver webDriver;

    public TakeAwayPage(WebDriver driver){
        super(driver);
    }

    //WebElements-------------------------

    @FindBy(how = How.NAME, using = "mysearchstring")
    public WebElement addressSearch;

    @FindBy(how = How.ID, using = "submit_deliveryarea")
    public WebElement showButton;

    @FindBy(how = How.XPATH, using = "//a[@id='reference']/span")
    public WebElement alpha8888;

    @FindBy(how = How.XPATH, using = "/html/body/header/div[2]/div/div/div[1]/h1[2]/span")
    public WebElement numberListedRestaurants;

    @FindBy(how = How.LINK_TEXT, using = "Bas Burgers")
    public WebElement restaurantBasBurger;

    @FindBy(how = How.LINK_TEXT, using = "Automated Scoober Restaurant")
    public WebElement restaurantAutomatedScooberRestaurant;

    @FindBy(how = How.LINK_TEXT, using = "Androidlicious")
    public WebElement restaurantAndroidlicious;

    @FindBy(how = How.XPATH, using = "/html/body/main/div/div/div[1]/div[3]/div/div[2]/div[2]/div[1]/div[1]/div[1]")
    public WebElement hamburger;

    @FindBy(how = How.XPATH, using = "/html/body/main/div/div/div[1]/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/div/div/div/div[2]/form/div/div[2]/button/span/h3")
    public WebElement burgerPrice;

    @FindBy(how = How.XPATH, using = "/html/body/main/div/div/div[1]/div[3]/div/div[2]/div[2]/div[1]/div[1]/div[1]/div[2]/span/span")
    public WebElement salami;


    @FindBy(how = How.XPATH, using = "/html/body/header/div[4]/div[2]/button")
    public WebElement orderButton;



    //Methods--------------------------------

    public  void searchTextField(String text){
        LOGGER.info("Entering search address " + text);
        typeText(addressSearch, text);
    }

    public void clickShowButton(){
        LOGGER.info("Clicking the show button");
        click(showButton);
    }

     public void selectAlphaAddress(){
        LOGGER.info("Choosing 8888 Alpha address");
         click(alpha8888);
    }

    public void verifyRestaurantsList(){
        LOGGER.info("Verifying List with restaurants");

        waitForElementVisibility(restaurantBasBurger);
        waitForElementToBeClickable(restaurantAutomatedScooberRestaurant);

        Assertions.assertEquals(true, restaurantBasBurger.isDisplayed());
        Assertions.assertEquals(true, restaurantAutomatedScooberRestaurant.isDisplayed());
        Assertions.assertEquals(true, restaurantAndroidlicious.isDisplayed());
    }

    public void selectRestaurantBasBurger(){
        LOGGER.info("Selecting Bas Burger Restaurant");
        click(restaurantBasBurger);
    }

    public void selectAutomatedScooberRestaurant(){
        LOGGER.info("Selecting Automated Scoober Restaurant");
        click(restaurantAutomatedScooberRestaurant);
    }

    public void selectHamburger(){
        LOGGER.info("Selecting Bas Burger Restaurant");
        click(hamburger);
    }

    public void selectSalami(){
        LOGGER.info("Selecting salami");
        click(salami);
    }

    public void clickBurgerPrice(){
        LOGGER.info("Clicking Burger Price");
        mouseOver(burgerPrice);
        burgerPrice.click();
    }

    public void clickOrderButton(){
        LOGGER.info("Clicking Order Button");
        click(orderButton);
    }
}
