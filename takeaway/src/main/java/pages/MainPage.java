package pages;

import base.BaseTools;
import org.openqa.selenium.WebDriver;

public class MainPage extends BaseTools {

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public void openMainPage() {
        LOGGER.info("Navigate to: " + BASE_URL);
        navigateTo(BASE_URL);
    }


}
