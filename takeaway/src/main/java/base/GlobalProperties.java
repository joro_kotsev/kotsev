package base;

import lombok.Data;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@Data
public class GlobalProperties {

    private static GlobalProperties instance;

    private final String baseUrl;
    private final String browser;
    private final String address;
    private final String postcode;
    private final String city;
    private final String name;
    private final String phonenum;
    private final String email;


    public GlobalProperties() throws IOException {

        Properties prop=new Properties();

        try (FileInputStream fis=new FileInputStream(System.getProperty("user.dir") + "/src/main/resources/"  + "takeAway.properties")) {
            prop.load(fis);
        }

        // read Test resources folder
        baseUrl=prop.getProperty("base.url");
        browser=prop.getProperty("browser");
        address=prop.getProperty("address");
        postcode=prop.getProperty("postcode");
        city=prop.getProperty("city");
        name=prop.getProperty("name");
        phonenum=prop.getProperty("phonenum");
        email=prop.getProperty("email");

    }

    public static GlobalProperties getInstance() {

        if (instance == null) {
            try {
                instance = new GlobalProperties();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return instance;
    }

}
