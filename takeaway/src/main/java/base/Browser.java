package base;

import lombok.Data;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

@Data
public class Browser {

    private String CHROME_DRIVER_PATH = "src\\main\\resources\\chromedriver.exe";
    private String FIREFOX_DRIVER_PATH = "src\\main\\resources\\geckodriver.exe";

    private BaseTools baseTools = new BaseTools();

    public void initFirefox() {
        System.setProperty("webdriver.gecko.driver", FIREFOX_DRIVER_PATH);
        baseTools.driver = new FirefoxDriver();
        baseTools.driver.manage().window().maximize();
        baseTools.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void initChrome(){
        System.setProperty("webdriver.chrome.driver", CHROME_DRIVER_PATH);
        baseTools.driver = new ChromeDriver();
        baseTools.driver.manage().window().maximize();
        baseTools.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void initBrowser(){

        String browserName = GlobalProperties.getInstance().getBrowser();

        if(browserName.equals("chrome")){
            initChrome();
        }
        else if(browserName.equals("Firefox")){
            initFirefox();
        }
    }

    public void tearDown() {
        baseTools.driver.quit();
    }

}
