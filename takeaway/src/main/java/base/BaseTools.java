package base;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;


public class BaseTools {

    public static final Logger LOGGER = LoggerFactory.getLogger(BaseTools.class);
    protected final String BASE_URL = GlobalProperties.getInstance().getBaseUrl();

    public WebDriver driver;

    public BaseTools(WebDriver driver,String country) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public BaseTools() {

    }

    public BaseTools(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

   public void navigateTo(String url) {
       driver.navigate().to(url);
    }

    public void click(WebElement element) {
        waitForElementVisibility(element);
        element.click();
    }

    public void typeText(WebElement element, String text) {
        waitForElementVisibility(element);
        element.clear();
        element.sendKeys(text);
    }

    public void clickWithActionsBuilder (WebElement element){
        Actions builder=new Actions(driver);
        builder.moveToElement(element).perform();
    }

    public void clearTextAndEnterDateWithActionsBuilder (WebElement element, String text){
        Actions builder=new Actions(driver);
        builder.click(element)
                .pause(500).keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL)
                .pause(500).sendKeys(Keys.BACK_SPACE)
                .pause(500).sendKeys(text).perform();
    }


    public void selectRadioButton(List<WebElement> allRadioButtons, String optionVisibleTextToSelect) {
        for (WebElement curRadioButton : allRadioButtons) {
            if (curRadioButton.getText().toLowerCase().contains(optionVisibleTextToSelect.toLowerCase())) {
                curRadioButton.click();
                return;
            }
        }

        throw new RuntimeException("Was unable to find a radio button with the specified text: " + optionVisibleTextToSelect);
    }

    protected void scrollDownToElement(WebElement element) {
        LOGGER.info("Scrolling down");
        JavascriptExecutor js = (JavascriptExecutor) driver;
        //This will scroll the page till the element is found
        js.executeScript("arguments[0].scrollIntoView();", element);
    }

    protected void mouseOver(WebElement element) {
        LOGGER.info("Scrolling down");
        Actions action = new Actions(driver);
        //This will perform mouseover function
        action.moveToElement(element);
    }

    protected WebElement waitForElement(WebElement element) {
        try {
            // To use WebDriverWait(), we would have to nullify
            // implicitlyWait().
            // Because implicitlyWait time also set "driver.findElement()" wait
            // time.
            // info from:
            // https://groups.google.com/forum/?fromgroups=#!topic/selenium-users/6VO_7IXylgY
            driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS); // nullify
            // implicitlyWait()

            WebDriverWait wait = new WebDriverWait(driver, 25);
            WebElement foundElementAfterWait = wait.until(ExpectedConditions.visibilityOfElementLocated((By) element));

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); // reset
            // implicitlyWait
            return element; // return the element
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public WebElement waitForElementPresent(WebElement element) {
        try {
            driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS); // nullify
            // implicitlyWait()

            WebDriverWait wait = new WebDriverWait(driver, 25);
            WebElement elementPresnted = wait.until(ExpectedConditions.presenceOfElementLocated((By) element));

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); // reset
            // implicitlyWait
            return elementPresnted; // return the element
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    protected WebElement waitForElementToBeClickable(WebElement elementToBeVisible) {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver, 25);

        WebElement foundElementAfterWait = wait.until(ExpectedConditions.elementToBeClickable(elementToBeVisible));
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        return foundElementAfterWait;
    }

    protected WebElement waitForElementVisibility(WebElement elementToBeVisible) {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver, 25);

        WebElement foundElementAfterWait = wait.until(ExpectedConditions.visibilityOf(elementToBeVisible));
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        return foundElementAfterWait;
    }

    public void sleepMiliseconds(int timeInMillisecond) {

        LOGGER.info("Waiting " + timeInMillisecond / 1000 + " seconds");
        try {

            Thread.sleep(timeInMillisecond);
        } catch (Exception a) {
            throw new RuntimeException("Couldn't wait time  in milliseconds " + timeInMillisecond);
        }
    }

    public void switchToAlert() {
        driver.switchTo().alert().accept();
    }


}