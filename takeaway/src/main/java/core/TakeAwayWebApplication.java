package core;

import base.Browser;
import pages.AddressPage;
import pages.MainPage;
import pages.TakeAwayPage;

public class TakeAwayWebApplication {

    private Browser browser;
    private TakeAwayPage takeAwayPage;
    private MainPage mainPage;
    private AddressPage addressPage;

    public TakeAwayWebApplication() {
    }

    //lazy instantiating methods

    public Browser browser() {
        if (browser == null) {
            browser=new Browser();
            return browser;
        } else {
            return browser;
        }
    }

    public TakeAwayPage takeAwayPage() {
        if (takeAwayPage == null) {
            takeAwayPage=new TakeAwayPage(browser.getBaseTools().driver);
            return takeAwayPage;
        } else {
            return takeAwayPage;
        }
    }

    public MainPage mainPage() {
        if (mainPage == null) {
            mainPage=new MainPage(browser.getBaseTools().driver);
            return mainPage;
        } else {
            return mainPage;
        }
    }

    public AddressPage addressPage() {
        if (addressPage == null) {
            addressPage= new AddressPage(browser.getBaseTools().driver);
            return addressPage;
        } else {
            return addressPage;
        }
    }


}
